# DRK Musterseiten

Hilfe zu den Musterseiten finden Sie hier:

* https://web-handbuch.drk-intern.de/Index.html
* https://www.drk-intern.de/musterseiten/musterseiten.html

Die zur kompletten Installation nötigen Dateien und Datenbankdumps erhalten Sie
bei der DRK Service GmbH

* https://www.drk-intern.de/musterseiten/download-anfrage/musterwebseite-als-downloaddump.html
